import { Component, OnInit, OnDestroy, DoCheck} from '@angular/core';
import { MainService } from 'src/app/services/main.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-array-view',
  templateUrl: './array-view.component.html',
  styles: []
})
export class ArrayViewComponent implements OnInit, OnDestroy, DoCheck {
  private storageId = 'DATA_ARRAY';

  data: number[] = [];
  isLoading = false;
  error = false;
  numbers$: Subscription;

  constructor(private service: MainService) { }

  ngOnInit() {
    this.data = this.service.getDataStorage(this.storageId) as number[] || [];
  }

  ngDoCheck() {
    this.service.setAlertState(this.isLoading, this.error, this.data);
  }

  get isLoadingStatus() {
    return this.isLoading;
  }

  /**
   * Clean data from session storage.
   */
  cleanData() {
    this.data = [];
    this.error = false;
    this.service.cleanDataStorage(this.storageId);
  }

  /**
   * Set error state.
   */
  setError() {
    this.isLoading = false;
    this.error = true;
    this.data = [];
  }

  /**
   * Method that loads data in the component.
   */
  getData(): void {
    this.isLoading = true;
    this.numbers$ = this.service.getData('array.php')
      .subscribe(data => {
        this.isLoading = false;
        this.cleanData();
        if (data.success) {
          this.error = false;
          this.data = [...data.data];
          this.service.saveDataStorage(this.storageId, this.data);
        } else {
          this.setError();
        }
      }, () => {
        this.setError();
      });
  }

  ngOnDestroy() {
    if (this.numbers$) {
      this.numbers$.unsubscribe();
    }
  }
}
