import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-array-raw-data',
  templateUrl: './array-raw-data.component.html',
  styles: []
})
export class ArrayRawDataComponent implements OnInit {

  @Input() data: number[];

  constructor() { }

  ngOnInit() {
  }

}
