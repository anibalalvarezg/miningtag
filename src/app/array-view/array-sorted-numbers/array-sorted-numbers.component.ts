import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-array-sorted-numbers',
  templateUrl: './array-sorted-numbers.component.html',
  styles: []
})
export class ArraySortedNumbersComponent implements OnInit {

  @Input() data: number[] = [];
  sortedNumbers: number[] = [];
  constructor() { }

  ngOnInit() {
    this.sortedNumbers = this.quickSort([...new Set(this.data)]) || [];
  }

  /**
   * Quicksort sorting algorithm
   * @param numbers: Array of numbers.
   * @returns numbers: Array sorted.
   */
  quickSort(numbers: number[]): number[] {
    if (numbers.length === 0) {
      return [];
    }
    const left = [];
    const right = [];
    const pivot = numbers[0];
    for(let i = 1; i < numbers.length; i++) {
      if (numbers[i] < pivot) {
        left.push(numbers[i]);
      } else {
        right.push(numbers[i]);
      }
    }
    return [...this.quickSort(left), pivot, ...this.quickSort(right)];
  }

}
