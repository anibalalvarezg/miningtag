import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArrayViewComponent } from './array-view.component';
import { ArraySortedNumbersComponent } from './array-sorted-numbers/array-sorted-numbers.component';
import { ArrayViewRoutingModule } from './array-view.routing';
import { SharedModule } from '../Shared/shared.module';
import { ArrayProcessedDataComponent } from './array-processed-data/array-processed-data.component';
import { ArrayRawDataComponent } from './array-raw-data/array-raw-data.component';


@NgModule({
  declarations: [ArraySortedNumbersComponent, ArrayViewComponent, ArrayProcessedDataComponent, ArrayRawDataComponent],
  imports: [
    CommonModule,
    ArrayViewRoutingModule,
    SharedModule
  ]
})
export class ArrayViewModule { }
