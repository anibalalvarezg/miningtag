import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArrayViewComponent } from './array-view.component';

const routes: Routes = [
  { path: '', component: ArrayViewComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArrayViewRoutingModule { }
