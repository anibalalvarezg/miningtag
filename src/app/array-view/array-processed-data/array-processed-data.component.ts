import { Component, OnInit, Input } from '@angular/core';
import { INumber } from 'src/app/Shared/interfaces/numbers.interface';

@Component({
  selector: 'app-array-processed-data',
  templateUrl: './array-processed-data.component.html',
  styles: []
})
export class ArrayProcessedDataComponent implements OnInit {

  @Input() data: number[];
  processedData: INumber[];
  constructor() { }

  ngOnInit() {
    this.processedData = this.processData(this.data);
  }

  /**
   * Method that processes the data to be displayed in the template.
   *
   * @param numbers: Array of numbers from service.
   * @returns INumber[]: Array of numbers processed.
   */
  processData(numbers: number[]): INumber[] {
    if (numbers.length > 0) {
      const processedData = [];
      const listOfNumbers = [...new Set(numbers)];

      listOfNumbers.forEach(num => {
        const quantity = numbers.filter(el => el === num).length;
        const lastPosition = numbers.lastIndexOf(num) + 1;
        const firstPosition = numbers.findIndex(n => n ===  num) + 1;
        const numberToAdd: INumber = {
          number: num,
          quantity,
          firstPosition,
          lastPosition,
        };
        processedData.push(numberToAdd);
      });

      return processedData;
    } else {
      return [];
    }
  }

}
