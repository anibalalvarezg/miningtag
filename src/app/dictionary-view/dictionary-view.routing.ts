import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DictionaryViewComponent } from './dictionary-view.component';

const routes: Routes = [
  { path: '', component: DictionaryViewComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DictionaryViewRoutingModule { }
