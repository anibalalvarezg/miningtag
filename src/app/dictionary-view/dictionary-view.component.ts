import { Component, OnInit, OnDestroy, DoCheck } from '@angular/core';
import { MainService } from 'src/app/services/main.service';
import { Subscription } from 'rxjs';

export interface IDictionary {
  paragraph: string;
}

export interface IDictionaryData {
  frequency: number[];
  sum: number;
}

@Component({
  selector: 'app-dictionary-view',
  templateUrl: './dictionary-view.component.html',
  styles: []
})
export class DictionaryViewComponent implements OnInit, OnDestroy, DoCheck {
  private storageId = 'ARRAY_DICTIONARY';

  data: string[] = [];
  isLoading = false;
  error = false;
  dict$: Subscription;

  constructor(private service: MainService) { }

  ngOnInit() {
    this.data = this.service.getDataStorage(this.storageId) as string[] || [];
  }

  ngDoCheck() {
    this.service.setAlertState(this.isLoading, this.error, this.data);
  }

  get isLoadingStatus() {
    return this.isLoading;
  }

  /** Clean data from session storage.
   */
  cleanData() {
    this.data = [];
    this.error = false;
    this.service.cleanDataStorage(this.storageId);
  }

  /**
   * Set error state.
   */
  setError() {
    this.isLoading = false;
    this.error = true;
    this.data = [];
  }

  /**
   * Method that loads data in the component.
   */
  getData(): void {
    this.isLoading = true;
    this.dict$ = this.service.getData('dict.php')
      .subscribe((data) => {
        this.isLoading = false;
        this.cleanData();
        if (data.success) {
          this.error = false;
          this.data = JSON.parse(data.data).map(el => el.paragraph.toLocaleLowerCase());
          this.service.saveDataStorage(this.storageId, this.data);
        } else {
          this.setError();
        }
    }, () => {
      this.setError();
    });
  }

  ngOnDestroy() {
    if (this.dict$) {
      this.dict$.unsubscribe();
    }
  }
}
