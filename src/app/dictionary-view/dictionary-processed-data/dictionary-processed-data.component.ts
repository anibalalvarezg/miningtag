import { Component, OnInit, Input } from '@angular/core';
import { IDictionaryData } from '../dictionary-view.component';

@Component({
  selector: 'app-dictionary-processed-data',
  templateUrl: './dictionary-processed-data.component.html',
  styles: []
})
export class DictionaryProcessedDataComponent implements OnInit {
  @Input() data: string[] = [];

  alphabet: string[] = [];
  processedData: IDictionaryData[] = [];

  constructor() { }

  ngOnInit() {
    this.alphabet = [...Array(26)].map((_, y) => String.fromCharCode(y + 65).toLocaleLowerCase());
    this.processedData = this.processData(this.data);
  }

  /**
   * Method that processes the data to be displayed in the template.
   * @param data: Array of paragraphs.
   * @returns IDictionaryData[]: Array of letters frequency and sum of numbers contained.
   */
  processData(data: string[]): IDictionaryData[] {
    const procesedData = [];
    data.forEach((paragraph) => {
      const frequency = [];
      this.alphabet.forEach(letter => {
        frequency.push((paragraph.match(new RegExp(letter, 'g')) || []).length);
      });
      const numbers = paragraph.match(/\d+/g) || [];
      const sum = numbers.map((num: string) => +num).reduce((a, b) => a + b, 0);
      procesedData.push({ frequency, sum });
    });
    return procesedData;
  }

}
