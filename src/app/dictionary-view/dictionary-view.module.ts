import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../Shared/shared.module';
import { DictionaryViewRoutingModule } from './dictionary-view.routing';
import { DictionaryViewComponent } from './dictionary-view.component';
import { DictionaryRawDataComponent } from './dictionary-raw-data/dictionary-raw-data.component';
import { DictionaryProcessedDataComponent } from './dictionary-processed-data/dictionary-processed-data.component';



@NgModule({
  declarations: [DictionaryViewComponent, DictionaryRawDataComponent, DictionaryProcessedDataComponent],
  imports: [
    CommonModule,
    SharedModule,
    DictionaryViewRoutingModule
  ]
})
export class DictionaryViewModule { }
