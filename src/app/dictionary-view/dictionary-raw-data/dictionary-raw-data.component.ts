import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-dictionary-raw-data',
  templateUrl: './dictionary-raw-data.component.html',
  styles: []
})
export class DictionaryRawDataComponent implements OnInit {
  @Input() data: string[] = [];
  viewRawData = true;

  constructor() { }

  ngOnInit() {
  }

}
