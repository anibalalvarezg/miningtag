import { Component, OnInit, OnDestroy } from '@angular/core';
import { MainService } from 'src/app/services/main.service';
import { Subscription } from 'rxjs';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styles: []
})
export class AlertComponent implements OnInit, OnDestroy {

  type: string;
  alert$: Subscription;
  inView = false;

  constructor(private service: MainService) { }

  ngOnInit() {
    this.inView = true;
    this.initListeners();
    this.type = '';
  }

  initListeners(): void {
    this.alert$ = this.service.alertState$
      .pipe(
        takeWhile(() => this.inView),
      )
      .subscribe((state: string) => {
        this.type = state;
      });
  }

  ngOnDestroy() {
    this.inView = false;
    if (this.alert$) {
      this.alert$.unsubscribe();
    }
  }

}
