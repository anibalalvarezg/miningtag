import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: []
})
export class HeaderComponent {
  @Input() title: string;
  @Input() isLoading: boolean;
  @Input() data: string[] | number[];
  @Output() execGetData: EventEmitter<boolean> = new EventEmitter();
  @Output() execCleanData: EventEmitter<boolean> = new EventEmitter();
  constructor() {
  }

  getData(): void {
    this.execGetData.next(true);
  }

  cleanData(): void {
    this.execCleanData.next(true);
  }

  get dataLength(): number {
    return this.data.length;
  }

}
