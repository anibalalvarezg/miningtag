export interface INumber {
  number: number;
  quantity: number;
  firstPosition: number;
  lastPosition: number;
}
