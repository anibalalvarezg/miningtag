export interface IAlert {
  title: string;
  icon: string;
  description?: string;
  type: string;
}
