import { Component, OnInit, Input } from '@angular/core';
import { IAlert } from '../interfaces/alert.interface';
import { ALERTS } from '../enums/alerts.enum';

const ALERTS_TEMPLATE = {
  loading: {
    title: 'Loading...',
    icon: 'fa-sync-alt fa-spin',
    description: 'Wait please.',
    type: 'alert-info'
  },
  empty: {
    title: 'No data.',
    icon: 'fa-exclamation-triangle',
    type: 'alert-warning'
  },
  error: {
    title: 'Error loading data.',
    icon: 'fa-exclamation',
    type: 'alert-danger'
  }

}

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styles: []
})
export class AlertsComponent implements OnInit {

  @Input() type: string;

  alert: IAlert;

  constructor() { }

  ngOnInit() {
    this.alert = this.getAlertTemplate(this.type);
  }

  /**
   * Method that returns alert template.
   *
   * @param string Type of alert.
   * @returns IAlert Template alert
   */
  getAlertTemplate(type: string): IAlert {
    switch (ALERTS[type]) {
      case ALERTS.LOADING:
        return ALERTS_TEMPLATE[ALERTS.LOADING];
      case ALERTS.ERROR:
        return ALERTS_TEMPLATE[ALERTS.ERROR];
      case ALERTS.EMPTY:
        return ALERTS_TEMPLATE[ALERTS.EMPTY];
    }
  }

}
