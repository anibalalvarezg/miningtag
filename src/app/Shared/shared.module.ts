import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { AlertsComponent } from './alerts/alerts.component';
import { AlertComponent } from './alert/alert.component';

@NgModule({
  declarations: [AlertComponent, AlertsComponent, HeaderComponent],
  imports: [
    CommonModule
  ],
  exports : [AlertComponent, AlertsComponent, HeaderComponent],
})
export class SharedModule { }
