export enum ALERTS  {
  LOADING = 'loading',
  ERROR = 'error',
  EMPTY = 'empty',
}
