import { Injectable } from '@angular/core';
import { IData } from '../Shared/interfaces/data.interface';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { take } from 'rxjs/operators';
import { INumber } from '../Shared/interfaces/numbers.interface';

@Injectable({
  providedIn: 'root'
})
export class MainService {
  private baseUrl = 'http://patovega.com/prueba_frontend/';
  public alertState$ = new Subject<string>();
  constructor(private http: HttpClient) { }

  getData(path: string): Observable<IData> {
    return this.http.get<IData>(`${this.baseUrl + path}`)
      .pipe(take(1));
  }

  saveDataStorage(id: string, data: number[] | string[]): void {
    sessionStorage.setItem(id, JSON.stringify(data));
  }

  getDataStorage(id: string): number[] | string[] {
    return JSON.parse(sessionStorage.getItem(id));
  }

  cleanDataStorage(id: string): void {
    sessionStorage.removeItem(id);
  }

  /**
   * Set alert state.
   */
  setAlertState(isLoading: boolean, error: boolean, data: number[] | string[]) {
    if (isLoading && !error) {
      this.alertState$.next('LOADING');
    } else if (!isLoading && data.length === 0 && !error) {
      this.alertState$.next('EMPTY');
    } else if (!isLoading && error) {
      this.alertState$.next('ERROR');
    } else {
      this.alertState$.next('');
    }
  }
}
