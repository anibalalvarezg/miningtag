import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const lazyArrayModule = () => import('./array-view/array-view.module').then(m => m.ArrayViewModule);

const routes: Routes = [
  { path: '',  redirectTo: 'array', pathMatch: 'full' },
  { path: 'array', loadChildren: lazyArrayModule },
  { path: 'dict', loadChildren: () => import('./dictionary-view/dictionary-view.module').then(m => m.DictionaryViewModule) },
  { path: '**', loadChildren: lazyArrayModule},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
