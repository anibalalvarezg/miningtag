# Miningtag

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.2.

## External libraries 

Libraries used by CDN: 

*Bootstrap 

*Fontawesome

*Animate.css

## Instalation and run server

Run `npm install` or `npm i` for install dependencies.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


